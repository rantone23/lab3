create sequence car_seq start 1 increment 1 no maxvalue;

create table car (
    id bigint not null primary key default nextval('car_seq'),
    mark varchar not null,
    number varchar not null
);

alter sequence car_seq owned by car.id;

