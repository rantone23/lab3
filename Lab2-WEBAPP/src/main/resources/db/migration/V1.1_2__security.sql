create sequence user_seq start 1 increment 1 no maxvalue;

create table users (
    id int not null primary key,
    username varchar not null,
    password varchar not null,
    role varchar not null
);

alter sequence user_seq owned by users.id;