package org.practice.autopark.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("exception")
public class ExceptionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("exceptionMessage", req.getAttribute("javax.servlet.error.status_code")
                + ": " + req.getAttribute("javax.servlet.error.message"));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/pages/error.jsp");
        requestDispatcher.forward(req, resp);
    }
}
