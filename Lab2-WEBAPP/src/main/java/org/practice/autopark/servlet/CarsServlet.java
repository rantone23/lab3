package org.practice.autopark.servlet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.practice.autopark.service.CarService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet("cars")
public class CarsServlet extends HttpServlet {

    @EJB
    private CarService carService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("cars", carService.findCarEntities());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/pages/cars.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DeletionRequest deletionRequest = receiveDeletionCars(req);
        if (deletionRequest != null && deletionRequest.isCarIdentifiersPresent())
            carService.deleteCars(deletionRequest.getCarIdentifiers());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SavingCarRequest savingCarRequest = receiveCreationRequest(req);
        if (savingCarRequest != null && savingCarRequest.isNotEmptyStructure()) {
            carService.saveCar(savingCarRequest);
        }
    }

    private SavingCarRequest receiveCreationRequest(HttpServletRequest req) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(req.getInputStream(), SavingCarRequest.class);
        } catch (IOException exception) {
            return null;
        }
    }

    protected DeletionRequest receiveDeletionCars(HttpServletRequest httpServletRequest) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(httpServletRequest.getInputStream(), DeletionRequest.class);
        } catch (IOException e) {
            return null;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DeletionRequest {
        private List<Long> carIdentifiers = new LinkedList<Long>();

        public List<Long> getCarIdentifiers() {
            return carIdentifiers;
        }

        public void setCarIdentifiers(List<Long> carIdentifiers) {
            this.carIdentifiers = carIdentifiers;
        }

        public boolean isCarIdentifiersPresent() {
            return carIdentifiers != null && !carIdentifiers.isEmpty();
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SavingCarRequest {

        private Long id;
        private String carMark;
        private String carNumber;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getCarMark() {
            return carMark;
        }

        public void setCarMark(String carMark) {
            this.carMark = carMark;
        }

        public String getCarNumber() {
            return carNumber;
        }

        public void setCarNumber(String carNumber) {
            this.carNumber = carNumber;
        }

        public boolean isNotEmptyStructure() {
            return carMark != null && carNumber != null;
        }
    }
}
