package org.practice.autopark.exception;

public class StartupException extends RuntimeException {

    public StartupException() {
    }

    public StartupException(String message) {
        super(message);
    }

    public StartupException(String message, Throwable cause) {
        super(message, cause);
    }

    public StartupException(Throwable cause) {
        super(cause);
    }

}
