package org.practice.autopark.db;

public interface MigrationSupport {
    void migrate();
}
