package org.practice.autopark.service.impl;

import org.practice.autopark.service.PasswordHashGenerator;

import javax.ejb.Local;
import javax.ejb.Singleton;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Singleton
@Local(PasswordHashGenerator.class)
public class MD5PasswordHashGenerator implements PasswordHashGenerator {

    public byte[] generateHash(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(password.getBytes("UTF-8"));
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
