package org.practice.autopark.service;

import org.practice.autopark.persistence.entity.CarEntity;
import org.practice.autopark.servlet.CarsServlet;

import java.util.List;

public interface CarService {
    List<CarEntity> findCarEntities();

    void deleteCars(List<Long> carIdentifiers);

    void saveCar(CarsServlet.SavingCarRequest savingCarRequest);
}
