package org.practice.autopark.service.impl;

import org.practice.autopark.persistence.entity.CarEntity;
import org.practice.autopark.service.CarService;
import org.practice.autopark.servlet.CarsServlet;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateful
@Local(CarService.class)
public class CarServiceImpl implements CarService {

    @PersistenceContext(unitName = "AutoPark")
    private EntityManager entityManager;

    public List<CarEntity> findCarEntities() {
        TypedQuery<CarEntity> namedQuery = entityManager.createNamedQuery("Car.findAll", CarEntity.class);
        return namedQuery.getResultList();
    }

    public void deleteCars(List<Long> carIdentifiers) {
        Query namedQuery = entityManager.createNamedQuery("Car.deleteByIdentifiers");
        namedQuery.setParameter("identifiers", carIdentifiers);
        namedQuery.executeUpdate();
    }

    public void saveCar(CarsServlet.SavingCarRequest savingCarRequest) {
        createCar(toEntity(savingCarRequest));
    }

    private CarEntity toEntity(CarsServlet.SavingCarRequest savingCarRequest) {
        CarEntity carEntity = new CarEntity();
        carEntity.setId(savingCarRequest.getId());
        carEntity.setMark(savingCarRequest.getCarMark());
        carEntity.setNumber(savingCarRequest.getCarNumber());
        return carEntity;
    }

    public void createCar(CarEntity carEntity) {
        entityManager.merge(carEntity);
    }
}
