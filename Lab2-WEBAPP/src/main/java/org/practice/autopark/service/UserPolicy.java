package org.practice.autopark.service;

import org.practice.autopark.persistence.entity.UserEntity;

public interface UserPolicy {

    UserEntity findUser(String principal, String credentials);
}
