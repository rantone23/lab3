function authenticate() {
    $.ajax({
        url: "authenticate?j_username=" + $("#login").val() + "&j_password=" + $("#password").val(),
        method: "POST",
        dataType: "text",
        success: function (data) {
            window.location.replace("./")
        },
        error: function (data) {
            window.location.reload();
        }
    });
}